﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoltShop.Models;

namespace VoltShop.Controllers
{
    public class CarroController : Controller
    {
        // GET: Carro
        public ActionResult Index()
        {
            if (Session["carro"] == null)
                Session["carro"] = new List<Item>();

            return View();
        }

        public ActionResult Comprar(int id)
        {
            int num = 0;
            if (Session["itemTotal"] == null)
            {
                num = 0;
                Session["itemTotal"] = num;
            }
            else
            {
                num = int.Parse(Session["itemTotal"].ToString());
                num = num + 1;
                Session["itemTotal"] = num;
            }

            ProdCarro carro = new ProdCarro();
            if (Session["carro"] == null)
            {
                List<Item> cart = new List<Item>();
                producto P = carro.find(id);
                string nam = P.nombre;
                cart.Add(new Item { Product = carro.find(id), Cantidad = 1 });
                Session["carro"] = cart;
            }
            else
            {
                List<Item> cart = (List<Item>)Session["carro"];
                int index = isExist(id);
                if (index != -1)
                {
                    cart[index].Cantidad++;
                }
                else
                {
                    producto P = carro.find(id);
                    string nam = P.nombre;
                    cart.Add(new Item { Product = carro.find(id), Cantidad = 1 });
                }
                Session["carro"] = cart;
            }            
            return RedirectToAction("Index");
        }

        public ActionResult Quitar(int id)
        {
            int num = 0;
            if (Session["itemTotal"] == null)
            {
                num = 0;
                Session["itemTotal"] = num;
            }
            else
            {
                num = int.Parse(Session["itemTotal"].ToString());
                num = num - 1;
                Session["itemTotal"] = num;
            }

            List<Item> cart = (List<Item>)Session["carro"];
            int index = isExist(id);
            cart.RemoveAt(index);
            Session["carro"] = cart;
            return RedirectToAction("Index");
        }

        private int isExist(int id)
        {
            List<Item> carro = (List<Item>)Session["carro"];
            for (int i = 0; i < carro.Count; i++)
                if (carro[i].Product.Id.Equals(id))
                    return i;
            return -1;
        }
    }
}