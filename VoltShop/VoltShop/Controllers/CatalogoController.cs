﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VoltShop.Models;

namespace VoltShop.Controllers
{
    public class CatalogoController : Controller
    {
        private contextoVoltshop db = new contextoVoltshop();
        // GET: Catalogo
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BuscarProd(string nomBuscar)
        {
            ViewBag.SearchKey = nomBuscar;
            using (db)
            {
                var query = from st in db.producto
                            where st.nombre.Contains(nomBuscar)
                            select st;

                var listProd = query.ToList();
                ViewBag.Listado = listProd;
            }
            return View();
        }

        public ActionResult prodCategoria(int idCat)
        {
            List<producto> mercancia = null;
            var query = from p in db.producto
                        where p.id_categoria == idCat
                        select p;
            if (idCat == 1)
            {
                List<producto> iluminacion = query.ToList();
                mercancia = iluminacion;
                ViewBag.Catego = "Iluminación";
            }
            if (idCat == 2)
            {
                List<producto> herr = query.ToList();
                mercancia = herr;
                ViewBag.Catego = "Herramientas y accesorios";
            }
            if (idCat == 3)
            {
                List<producto> apagadores = query.ToList();
                mercancia = apagadores;
                ViewBag.Catego = "Apagadores y conectores";
            }
            if (idCat == 4)
            {
                List<producto> exten = query.ToList();
                mercancia = exten;
                ViewBag.Catego = "Extensiones y multicontactos";
            }
            if (idCat == 5)
            {
                List<producto> bases = query.ToList();
                mercancia = bases;
                ViewBag.Catego = "Bases de medición";
            }
            if (idCat == 6)
            {
                List<producto> dist = query.ToList();
                mercancia = dist;
                ViewBag.Catego = "Distribución eléctrica";
            }
            if (idCat == 7)
            {
                List<producto> conduit = query.ToList();
                mercancia = conduit;
                ViewBag.Catego = "Conduit, canalización, cajas";
            }
            ViewBag.productos = mercancia;
            return View();

        }

        //Método para obtener TODOS los productos
        public ActionResult todosProd()
        {
            List<producto> mercancia = null;
            var query = from p in db.producto
                        select p;
            mercancia = query.ToList();
            ViewBag.productos = mercancia;
            return View();
        }

        //método para obtener detalles del producto
        public ActionResult detallesProd(int? idProd, int idCat)
        {
            if (idProd == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.producto.Find(idProd);
            if (producto == null)
            {
                return HttpNotFound();
            }

            List<producto> mercancia = null;
            var query = from p in db.producto
                        where p.id_categoria == idCat
                        select p;
            if (idCat == 1)
            {
                List<producto> iluminacion = query.ToList();
                mercancia = iluminacion;
                ViewBag.Catego = "Iluminación";
            }
            if (idCat == 2)
            {
                List<producto> herr = query.ToList();
                mercancia = herr;
                ViewBag.Catego = "Herramientas y accesorios";
            }
            if (idCat == 3)
            {
                List<producto> apagadores = query.ToList();
                mercancia = apagadores;
                ViewBag.Catego = "Apagadores y conectores";
            }
            if (idCat == 4)
            {
                List<producto> exten = query.ToList();
                mercancia = exten;
                ViewBag.Catego = "Extensiones y multicontactos";
            }
            if (idCat == 5)
            {
                List<producto> bases = query.ToList();
                mercancia = bases;
                ViewBag.Catego = "Bases de medición";
            }
            if (idCat == 6)
            {
                List<producto> dist = query.ToList();
                mercancia = dist;
                ViewBag.Catego = "Distribución eléctrica";
            }
            if (idCat == 7)
            {
                List<producto> conduit = query.ToList();
                mercancia = conduit;
                ViewBag.Catego = "Conduit, canalización, cajas";
            }
            ViewBag.productos = mercancia;


            return View(producto);
        }
    }
}