﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using VoltShop.Models;

namespace VoltShop.Controllers
{
    public class ClientesController : Controller
    {
        private contextoVoltshop db = new contextoVoltshop();

        // GET: Clientes
        public ActionResult Index()
        {
            return View(db.cliente.ToList());
        }

        // GET: Clientes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cliente cliente = db.cliente.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // GET: Clientes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clientes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "Id,nombre,email,calle_t,colonia_t,estado_t,num_tarj_cred")] cliente cliente)
        public ActionResult Create(string nombre, string email, string calle_t, string colonia_t, string estado_t, string num_tarj_cred, string tipoTaj, string mes, string anio, string CVV)
        {
            cliente cliente = new cliente();
            int id = 0;
            if(!(db.cliente.Max(c => (int?)c.Id) == null))
            {
                id = db.cliente.Max(c => c.Id);
            }
            else
            {
                id = 0;
            }
            id++;
            if(Tarjeta(num_tarj_cred, tipoTaj, mes, anio, CVV))
            {
                //Comunicarse con el sistema de pago
                if(validaPago(nombre, calle_t, colonia_t, estado_t, num_tarj_cred, mes, anio, CVV))
                {
                    cliente.Id = id;

                    cliente.nombre = nombre;
                    cliente.email = Session["correo"].ToString();
                    cliente.calle_t = calle_t;
                    cliente.colonia_t = colonia_t;
                    cliente.estado_t = estado_t;
                    cliente.num_tarj_cred = num_tarj_cred;
                    cliente.mes = int.Parse(mes);
                    cliente.anio = int.Parse(anio);
                    //cliente.CVV = int.Parse(CVV);
                    cliente.tipoTaj = tipoTaj;
                    direcciones_entrega dir = new direcciones_entrega();
                    dir.calle = calle_t;
                    dir.colonia = colonia_t;
                    dir.estado = estado_t;
                    dir.id_cliente = id;

                    db.cliente.Add(cliente);
                    db.direcciones_entrega.Add(dir);
                    db.SaveChanges();
                    string[] nombres = cliente.nombre.ToString().Split(' ');
                    Session["name"] = nombres[0];
                    Session["usr"] = cliente.nombre;

                    if (Session["CrearOrden"] != null)
                    {
                        if (Session["CrearOrden"].Equals("pend"))
                        {
                            return RedirectToAction("CrearOrden", "Pago");
                        }
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    return RedirectToAction("Invalida");
                }
            }
            else
            {
                return RedirectToAction("Invalida");
            }
            /*if (ModelState.IsValid)
            {
                db.cliente.Add(cliente);
                db.SaveChanges();
                string[] nombres = cliente.nombre.ToString().Split(' ');
                Session["name"] = nombres[0];
                Session["usr"] = cliente.nombre;
                return RedirectToAction("Index","Home");
            }*/

            return View(cliente);
        }

        // GET: Clientes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cliente cliente = db.cliente.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        private bool Tarjeta(string tarj, string tipo, string mes, string anio, string cvv)
        {
            //Llamar al método de luhn para verificar que es un número válido de tarjeta de crédito
            bool retorna = validaTarj(tarj);

            if (retorna)
            {
                if((tarj.StartsWith("4")) && (tipo.Equals("Visa")))
                {
                    retorna = true;
                }
                else
                {
                    if ((tarj.StartsWith("5")) && (tipo.Equals("Master")))
                    {
                        retorna = true;
                    }
                    else
                    {
                        if ((tarj.StartsWith("3")) && (tipo.Equals("American")))
                        {
                            retorna = true;
                        }
                        else
                        {
                            retorna = false;
                        }
                    }
                }
                DateTime hoy = new DateTime();
                if(Convert.ToInt32(mes) > hoy.Year)
                {
                    if(Convert.ToInt32(mes) > hoy.Month)
                    {
                        retorna = true;
                    }
                    else
                    {
                        retorna = false;
                    }
                }
                else
                {
                    retorna = false;
                }
            }
            return retorna;
        }

        private bool validaTarj(string tarj)
        {
            bool retorna = true;
            StringBuilder digitsOnly = new StringBuilder();
            foreach(Char c in tarj)
            {
                if (Char.IsDigit(c)) digitsOnly.Append(c);
            };

            if (digitsOnly.Length > 18 || digitsOnly.Length < 15) return false;

            int sum = 0;
            int digit = 0;
            int addend = 0;
            bool timesTwo = false;

            for(int i = digitsOnly.Length -1; i >= 0; i--)
            {
                digit = Int32.Parse(digitsOnly.ToString(i, 1));
                if (timesTwo)
                {
                    addend = digit * 2;
                    if(addend > 9)
                    {
                        addend -= 9;
                    }
                    else
                        addend = digit;

                    sum += addend;
                    timesTwo = !timesTwo;
                }
            }
            retorna = ((sum % 10) == 0);
            return retorna;
        }

        private bool validaPago(string nombre, string calle_t, string colonia_t, string estado_t, string num_tarj_cred, string mes, string anio, string CVV)
        {
            bool retorna = true;
            //Se debe comunica con el sistema de pago enviando los datos de la tajeta
            //y los datos del cliente, en este ejemplo debe incluirse el codigo postal
            return retorna;

        }

        public ActionResult Invalida()
        {
            return View();
        }

        public ActionResult BorraUser()
        {
            string idUser = User.Identity.GetUserId();
            return RedirectToAction("Delete", "Account", routeValues: new { id = idUser });
        }

        private bool validarTarj(string tarj)
        {
            int[] cardInt = new int[tarj.Length];
            for(int i=0; i < tarj.Length; i++)
            {
                cardInt[i] = (int)(tarj[i] - '0');
            }
            for(int i=cardInt.Length - 2; i>=0; i = i - 2)
            {
                int tempValue = cardInt[i];
                tempValue = tempValue * 2;
                if(tempValue > 9)
                {
                    tempValue = tempValue % 10 + 1;
                }
                cardInt[i] = tempValue;
            }

            int total = 0;
            for(int i = 0; i<cardInt.Length; i++)
            {
                total += cardInt[i];
            }
            if(total % 10 == 0)
            {
                return true;
            }
            return false;
        }

        // POST: Clientes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,nombre,email,calle_t,colonia_t,estado_t,num_tarj_cred")] cliente cliente)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cliente).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cliente);
        }

        // GET: Clientes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cliente cliente = db.cliente.Find(id);
            if (cliente == null)
            {
                return HttpNotFound();
            }
            return View(cliente);
        }

        // POST: Clientes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cliente cliente = db.cliente.Find(id);
            db.cliente.Remove(cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
