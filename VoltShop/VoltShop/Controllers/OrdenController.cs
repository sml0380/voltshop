﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VoltShop.Models;

namespace VoltShop.Controllers
{
    [Authorize]
    public class OrdenController : Controller
    {
        private contextoVoltshop db = new contextoVoltshop();

        // GET: Orden
        public ActionResult Index()
        {
            var orden_cliente = db.orden_cliente.Where(o => o.fecha_envio == null).OrderBy(o => o.fecha_creacion).Include(o => o.cliente).Include(o => o.paqueteria);
            return View(orden_cliente.ToList());
        }

        // GET: Orden
        public ActionResult Index1()
        {
            var orden_cliente = db.orden_cliente.Where(o => o.fecha_entrega == null && o.fecha_envio != null).OrderBy(o => o.fecha_creacion).Include(o => o.cliente).Include(o => o.paqueteria);
            return View(orden_cliente.ToList());
        }

        // GET: Orden/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orden_cliente orden_cliente = db.orden_cliente.Find(id);
            if (orden_cliente == null)
            {
                return HttpNotFound();
            }
            return View(orden_cliente);
        }

        // GET: Orden/Create
        public ActionResult Create()
        {
            ViewBag.id_cliente = new SelectList(db.cliente, "Id", "nombre");
            ViewBag.id_dir_entrega = new SelectList(db.direcciones_entrega, "Id", "calle");
            ViewBag.id_paqueteria = new SelectList(db.paqueteria, "Id", "Nombre");
            return View();
        }

        // POST: Orden/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,fecha_creacion,num_confirmacion,total,id_cliente,id_dir_entrega,id_paqueteria,num_guia,fecha_envio,fecha_entrega")] orden_cliente orden_cliente)
        {
            if (ModelState.IsValid)
            {
                db.orden_cliente.Add(orden_cliente);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_cliente = new SelectList(db.cliente, "Id", "nombre", orden_cliente.id_cliente);
            ViewBag.id_dir_entrega = new SelectList(db.direcciones_entrega, "Id", "calle", orden_cliente.id_dir_entrega);
            ViewBag.id_paqueteria = new SelectList(db.paqueteria, "Id", "Nombre", orden_cliente.id_paqueteria);
            return View(orden_cliente);
        }

        // GET: Orden/Edit/5
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orden_cliente orden_cliente = db.orden_cliente.Find(id);
            if (orden_cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_cliente = new SelectList(db.cliente, "Id", "nombre", orden_cliente.id_cliente);
            ViewBag.id_dir_entrega = new SelectList(db.direcciones_entrega, "Id", "calle", orden_cliente.id_dir_entrega);
            ViewBag.id_paqueteria = new SelectList(db.paqueteria, "Id", "Nombre", orden_cliente.id_paqueteria);
            return View(orden_cliente);
        }

        // GET: Orden/Edit/5
        [HttpGet]
        public ActionResult Edit1(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orden_cliente orden_cliente = db.orden_cliente.Find(id);
            if (orden_cliente == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_cliente = new SelectList(db.cliente, "Id", "nombre", orden_cliente.id_cliente);
            ViewBag.id_dir_entrega = new SelectList(db.direcciones_entrega, "Id", "calle", orden_cliente.id_dir_entrega);
            ViewBag.id_paqueteria = new SelectList(db.paqueteria, "Id", "Nombre", orden_cliente.id_paqueteria);
            return View(orden_cliente);
        }

        // POST: Orden/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,id_paqueteria,num_guia,fecha_envio")] orden_cliente orden_cliente)
        {
            if (ModelState.IsValid)
            {
                orden_cliente o = db.orden_cliente.Find(orden_cliente.Id);
                o.id_paqueteria = orden_cliente.id_paqueteria;
                o.num_guia = orden_cliente.num_guia;
                o.fecha_envio = orden_cliente.fecha_envio;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_cliente = new SelectList(db.cliente, "Id", "nombre", orden_cliente.id_cliente);
            ViewBag.id_dir_entrega = new SelectList(db.direcciones_entrega, "Id", "calle", orden_cliente.id_dir_entrega);
            ViewBag.id_paqueteria = new SelectList(db.paqueteria, "Id", "Nombre", orden_cliente.id_paqueteria);
            return View(orden_cliente);
        }

        // POST: Orden/Edit1/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit1([Bind(Include = "Id,id_paqueteria,fecha_entrega")] orden_cliente orden_cliente)
        {
            if (ModelState.IsValid)
            {
                orden_cliente o = db.orden_cliente.Find(orden_cliente.Id);
                o.fecha_entrega = orden_cliente.fecha_entrega;
                db.SaveChanges();
                return RedirectToAction("Index1");
            }
            ViewBag.id_cliente = new SelectList(db.cliente, "Id", "nombre", orden_cliente.id_cliente);
            ViewBag.id_dir_entrega = new SelectList(db.direcciones_entrega, "Id", "calle", orden_cliente.id_dir_entrega);
            ViewBag.id_paqueteria = new SelectList(db.paqueteria, "Id", "Nombre", orden_cliente.id_paqueteria);
            return View(orden_cliente);
        }

        // GET: Orden/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            orden_cliente orden_cliente = db.orden_cliente.Find(id);
            if (orden_cliente == null)
            {
                return HttpNotFound();
            }
            return View(orden_cliente);
        }

        // POST: Orden/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            orden_cliente orden_cliente = db.orden_cliente.Find(id);
            db.orden_cliente.Remove(orden_cliente);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
