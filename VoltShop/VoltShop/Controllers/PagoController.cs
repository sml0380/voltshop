﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Mvc;
using VoltShop.Models;

namespace VoltShop.Controllers
{
    [Authorize]
    public class PagoController : Controller
    {
        private contextoVoltshop db = new contextoVoltshop();
        private String NumConfirPago;

        // GET: Pago
        public ActionResult Index()
        {
            return View();
        }
        [AllowAnonymous]
        public ActionResult CrearOrden()
        {
            if (!User.Identity.IsAuthenticated)
            {
                Session["CrearOrden"] = "pend";
                return RedirectToAction("Login", "Account");
            }

            String correo = User.Identity.Name;
            
            string fechaCreacion = DateTime.Today.ToShortDateString();
            string fechaProbEntrega = DateTime.Today.AddDays(3).ToShortDateString();
            var cliente = (from c in db.cliente
                           where c.email == correo
                           select c).ToList().FirstOrDefault();

            Session["dirCliente"] = cliente.calle_t + " " + cliente.colonia_t + " " + cliente.estado_t;
            Session["fechaOrden"] = fechaCreacion;
            Session["fpEntreg"] = fechaProbEntrega;

            if (cliente.num_tarj_cred.StartsWith("4"))
                Session["tTarj"] = "1";
            if (cliente.num_tarj_cred.StartsWith("5"))
                Session["tTarj"] = "2";
            if (cliente.num_tarj_cred.StartsWith("3"))
                Session["tTarj"] = "3";

            Session["nTarj"] = cliente.num_tarj_cred;
            return View();
        }

        public ActionResult Pagar(string tipoPago)
        {
            string correo = User.Identity.Name;

            DateTime fechaCreacion = DateTime.Today;
            DateTime fechaProbEntrega = fechaCreacion.AddDays(3);
            var cliente = (from c in db.cliente
                           where c.email == correo
                           select c).ToList().FirstOrDefault();
            int idClient = cliente.Id;

            if (tipoPago.Equals("T"))
            {
                if (!validaPago(cliente))
                {
                    return RedirectToAction("pagoNoAceptado");
                }
                else
                {
                    var dirEnt = (from d in db.direcciones_entrega
                                  where d.id_cliente == cliente.Id
                                  select d).ToList().FirstOrDefault();

                    int idDir = dirEnt.Id;
                    return RedirectToAction("pagoAceptado", routeValues: new { idC = idClient, idD = idDir });
                }
            }

            if (tipoPago.Equals("P"))
            {
                var dirEnt = (from e in db.direcciones_entrega
                              where e.id_cliente == cliente.Id
                              select e).ToList().FirstOrDefault();

                int idDir = dirEnt.Id;
                validaPago(cliente);
                return RedirectToAction("pagoPaypal", routeValues: new { idC = idClient, idD = idDir });
             
            }
            return View();
        }

        public ActionResult pagoNoAceptado()
        {
            return View();
        }

        public ActionResult PagoPayPal(int idC, int idD)
        {

            Session["idDir"] = idD;

            Session["idClient"] = idC;
            return View();
        }

        public ActionResult pagandoPaypal(int idC, int idD)
        {

            Session["idDir"] = idD;

            Session["idClient"] = idC;
            return View();
        }

        public ActionResult pagoAceptado(int idC, int idD)
        {
            //Aceptado el pago creamos los datos de la orden y orden_producto
            orden_cliente oc = new orden_cliente();
            int id = 0;
            if (!(db.orden_cliente.Max(o => (int?)o.Id) == null))
            {

                id = db.orden_cliente.Max(o => o.Id);
            }
            else
            {
                id = 0;
            }
            id++;
            oc.Id = id;
            oc.fecha_creacion = DateTime.Today;
            //oc.fecha_envio = DateTime.Today;
            //oc.fecha_envio = DateTime.Today.AddDays(3);
            oc.num_confirmacion = Session["nConfirma"].ToString();
            var carro = Session["carro"] as List<Item>;
            var total = carro.Sum(item => item.Product.precio * item.Cantidad);
            oc.total = (int?)total;
            oc.id_cliente = idC;
            oc.id_dir_entrega = idD;
            oc.id_paqueteria = null;
            db.orden_cliente.Add(oc);
            db.SaveChanges();

            orden_producto ordenProd;
            List<orden_producto> listaProdOrd = new List<orden_producto>();
            foreach (Item linea in carro)
            {
                ordenProd = new orden_producto();
                ordenProd.id_orden = oc.Id;
                ordenProd.id_producto = linea.Product.Id;
                ordenProd.cantidad = linea.Cantidad;
                db.orden_producto.Add(ordenProd);
            }
            db.SaveChanges();

            Session["carro"] = null;
            Session["nConfirma"] = null;
            Session["itemTotal"] = 0;
            return View();
        }

        private bool validaPago(cliente cliente)
        {
            bool returna = true;
            //Se  debe comunicar con el sistema de pago enviando los datos de la tajeta
            //y los datos del cliente, en este ejemplo falta el código postal
            //El sistema debe regresar un núimero de confirmación
            //vamos a simularlo con un número aleatorio pero seguro
            int randomvalue;

            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                byte[] val = new byte[6];
                crypto.GetBytes(val);
                randomvalue = BitConverter.ToInt32(val, 1);
            }

            NumConfirPago = Math.Abs(randomvalue * 1000).ToString();
            Session["nConfirma"] = NumConfirPago;
            return returna;
        }
    }
}