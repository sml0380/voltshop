﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using VoltShop.Models;

namespace VoltShop.Controllers
{
    [Authorize]
    public class ProductosController : Controller
    {
        private contextoVoltshop db = new contextoVoltshop();

        // GET: Productos
        public ActionResult Index()
        {
            var producto = db.producto.Include(p => p.categoria);
            return View(producto.ToList());
        }

        // GET: Productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: Productos/Create
        public ActionResult Create()
        {
            ViewBag.id_categoria = new SelectList(db.categoria, "Id", "nombre");
            return View();
        }

        // POST: Productos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,nombre,descripcion,precio,imagen,existencia,stock,id_categoria,ultima_modificacion")] producto producto)
        {
            if (ModelState.IsValid)
            {
                db.producto.Add(producto);

                int id = producto.Id;
                var prod = db.producto.Find(id);
                DateTime hoy = DateTime.Now;
                prod.ultima_modificacion = hoy;

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.id_categoria = new SelectList(db.categoria, "Id", "nombre", producto.id_categoria);
            return View(producto);
        }

        // GET: Productos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            ViewBag.id_categoria = new SelectList(db.categoria, "Id", "nombre", producto.id_categoria);
            return View(producto);
        }

        // POST: Productos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,nombre,descripcion,precio,imagen,existencia,stock,id_categoria,ultima_modificacion")] producto producto)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(producto).State = EntityState.Modified;
                int id = producto.Id;
                var prod = db.producto.Find(id);

                string nombre_ant = prod.nombre;
                string nombre_act = prod.nombre;
                string desc_ant = prod.descripcion;
                string desc_act = prod.descripcion;
                decimal precio_ant = (decimal)prod.precio;
                decimal precio_act = (decimal)producto.precio;
                string img_ant = prod.imagen;
                string img_act = prod.imagen;
                int exis_ant = (int)prod.existencia;
                int exis_act = (int)prod.existencia;
                int stock_ant = (int)prod.stock;
                int stock_act = (int)prod.stock;

                prod.nombre = producto.nombre;
                prod.descripcion = producto.descripcion;
                prod.precio = producto.precio;
                prod.imagen = producto.imagen;
                prod.existencia = producto.existencia;
                prod.stock = producto.stock;
                prod.id_categoria = producto.id_categoria;

                if (precio_act != precio_ant || nombre_act != nombre_ant || desc_act != desc_ant || img_act != img_ant
                    || exis_act != exis_ant || stock_act != stock_ant)
                {
                    DateTime hoy = DateTime.Now;
                    prod.ultima_modificacion = hoy;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id_categoria = new SelectList(db.categoria, "Id", "nombre", producto.id_categoria);
            return View(producto);
        }

        // GET: Productos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            producto producto = db.producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            producto producto = db.producto.Find(id);
            db.producto.Remove(producto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
