﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoltShop.Models;

namespace VoltShop.Controllers
{
    [Authorize]
    public class UsuarioController : Controller
    {
        private contextoVoltshop bd = new contextoVoltshop();
        
        // GET: Usuario
        public ActionResult Index(string email)
        {
            if(User.Identity.IsAuthenticated)
            {
                string correo = email;
                string rol = "";

                using (bd)
                {
                    var query = from st in bd.empleados
                                where st.Email == correo
                                select st;
                    var lista = query.ToList();
                    if(lista.Count > 0)
                    {
                        var empleado = query.FirstOrDefault<empleados>();
                        string[] nombres = empleado.Nombre.ToString().Split(' ');
                        Session["name"] = nombres[0];
                        Session["usr"] = empleado.Nombre;
                        rol = empleado.Rol.ToString().TrimEnd();

                        if(HttpContext.Request.Cookies["usuario"] == null)
                        {
                            HttpCookie cookie = new HttpCookie("usuario");
                            cookie["rol"] = rol;
                            cookie["name"] = Session["name"].ToString();
                            cookie.Expires = DateTime.Now.AddDays(1);
                            Response.Cookies.Add(cookie);
                        }
                    }
                    else
                    {
                        var query1 = from st in bd.cliente
                                     where st.email == correo
                                     select st;
                        var lista1 = query1.ToList();
                        if (lista1.Count > 0)
                        {
                            cliente cliente = query1.FirstOrDefault<cliente>();
                            string[] nombres = cliente.nombre.ToString().Split(' ');
                            Session["name"] = nombres[0];
                            Session["usr"] = cliente.nombre;
                            rol = "cliente";

                            if(HttpContext.Request.Cookies["usuario"] == null)
                            {
                                HttpCookie cookie = new HttpCookie("usuario");
                                cookie["rol"] = rol;
                                cookie["name"] = Session["name"].ToString();
                                cookie["itemTotal"] = "0";
                                cookie.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(cookie);
                            }
                        }
                    }
                }
                if (rol == "RH")
                {
                    return RedirectToAction("Index", "RH");
                }
                if (rol == "paqueteria")
                {
                    return RedirectToAction("Index", "Envios");
                }
                if (rol == "almacen")
                {
                    return RedirectToAction("Index", "Almacen");
                }
                if (rol == "ventas")
                {
                    return RedirectToAction("Index", "Ventas");
                }
                if (rol == "admin")
                {
                    return RedirectToAction("Index", "Admin");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Home");
        }
    }
}