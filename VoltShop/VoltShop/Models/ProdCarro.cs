﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VoltShop.Models
{
    public class ProdCarro
    {
        private contextoVoltshop bd = new contextoVoltshop();
        private List<producto> products;
        public ProdCarro()
        {
            products = bd.producto.ToList();
        }

        public List<producto> findAll()
        {
            return this.products;
        }

        public producto find(int id)
        {
            producto pp = this.products.Single(p => p.Id.Equals(id));
            return pp;
        }
    }
}